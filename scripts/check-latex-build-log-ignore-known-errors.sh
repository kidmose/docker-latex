#/bin/sh
MYDIR="$(dirname "$(which "$0")")"
diff -u $MYDIR/known-errors.log <($MYDIR/check-latex-build-log.sh $@) `# compare log to already known errors`\
    | grep '^+' `# find error statements introduced with the new log` \
    | grep -v '^+++ /dev/fd' `# ignore the filename printed by diff` \
    | grep '.*' `# grep any new error line`

# invert return code
if [[ $? == 0 ]]; then exit 1; else exit 0; fi

