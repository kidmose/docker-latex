#!/bin/sh
# See https://github.com/blang/latex-docker/blob/master/latexdockercmd.sh
IMAGE=kidmose/latex

if test -z "$@"
then
    FILENAME="<FILENAME GOES HERE>"
else
    FILENAME="$@"
fi
TEX=${FILENAME}.tex

CMD="pdflatex $TEX &&\
     bibtex $FILENAME &&\
     pdflatex $TEX &&\
     pdflatex $TEX"

docker run --rm -ti --user="$(id -u):$(id -g)" --net=none -v "$PWD":/data "$IMAGE" "/bin/sh" "-c" "$CMD"
