#/bin/sh

if test -z "$@"
then
    FILENAME="<FILE NAME GOES HERE>"
else
    FILENAME="$@"
fi

cat $PWD/${FILENAME}.log | egrep -i 'error|warning'

# invert return code
if [[ $? == 0 ]]; then exit 1; else exit 0; fi
