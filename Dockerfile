FROM debian
MAINTAINER Egon Kidmose <kidmose@gmail.com>

ARG PKGS="https://gist.githubusercontent.com/kidmose/00dfd11e58e704476fd2dcb39b15c479/raw/00460c805a875db959187373770ba5f3be46e1cd/install-latex-from-apt-get.sh"

RUN DEBIAN_FRONTEND=noninteractive apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install -qy wget \
        && wget -q $PKGS \
        && chmod +x install-latex-from-apt-get.sh
RUN DEBIAN_FRONTEND=noninteractive ./install-latex-from-apt-get.sh \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /data
VOLUME ["/data"]
